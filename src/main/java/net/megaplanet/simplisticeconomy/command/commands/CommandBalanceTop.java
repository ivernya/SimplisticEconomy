package net.megaplanet.simplisticeconomy.command.commands;

import net.md_5.bungee.api.chat.TextComponent;
import net.megaplanet.simplisticeconomy.command.CommandBase;
import net.megaplanet.simplisticeconomy.command.CommandManager;
import net.megaplanet.simplisticeconomy.storage.IStorage;
import net.megaplanet.simplisticeconomy.util.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.text.DecimalFormat;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class CommandBalanceTop extends CommandBase {

    private final CommandManager commandManager;
    private static DecimalFormat df2 = new DecimalFormat("###,###.00");

    public CommandBalanceTop(CommandManager commandManager) {
        super("balancetop", "balance-top-command-description", "se.balancetop", null, 0, 1, new String[] {"baltop", "moneytop"});
        this.commandManager = commandManager;
    }

    @Override
    public void onCommand(CommandSender commandSender, String[] args) {
        int nPerPage = 10;
        int page = 1;

        if (args.length == 1) {
            try {
                page = Math.abs(Integer.parseInt(args[0]));
            } catch (NumberFormatException e) {
                commandSender.sendMessage(commandManager.getMessagesFile().getMessage("not-a-number")
                        .replace("%string%", args[0]));

                return;
            }
        }

        int offset = (page - 1) * nPerPage;

        int finalPage = page;

        new BukkitRunnable() {
            @Override
            public void run() {
                IStorage storage = commandManager.getPlugin().getStorageManager().getStorage();
                Map<String, Double> topBalance = storage.getTopBalance(nPerPage, offset);

                if (topBalance.isEmpty()) {
                    commandSender.sendMessage(commandManager.getMessagesFile().getMessage("balancetop-top-command-notEnoughOnThisPage"));
                    return;
                }

                AtomicInteger index = new AtomicInteger(offset + 1);

                Utils.sendCenteredMessage(commandSender, commandManager.getMessagesFile().getMessage("balance-top-command-header")
                        .replace("%page%", String.valueOf(finalPage)));

                topBalance.forEach((player, balance) -> {
                    String formattedBalance = df2.format(balance).replaceAll("\u00A0"," ");
                    commandSender.sendMessage(commandManager.getMessagesFile().getMessage("balance-top-command-player")
                            .replace("%index%", index.getAndIncrement() + "")
                            .replace("%player%", player)
                            .replace("%balance%", formattedBalance)
                            .replace("%currency%", commandManager.getPlugin().getStorageManager().getCurrency(balance)));
                });

                Utils.sendCenteredMessage(commandSender, commandManager.getMessagesFile().getMessage("balance-top-command-footer")
                        .replace("%page%", String.valueOf(finalPage + 1)));
            }

        }.runTaskAsynchronously(commandManager.getPlugin());
    }
}
