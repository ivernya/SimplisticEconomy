package net.megaplanet.simplisticeconomy.command.commands;

import com.gmail.tracebachi.DeltaRedis.Spigot.DeltaRedisApi;
import net.megaplanet.simplisticeconomy.command.CommandBase;
import net.megaplanet.simplisticeconomy.command.CommandManager;
import net.megaplanet.simplisticeconomy.storage.IStorage;
import net.megaplanet.simplisticeconomy.storage.UnknownAccountException;
import net.megaplanet.simplisticeconomy.util.CommandUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSet extends CommandBase {

    private final CommandManager commandManager;

    public CommandSet(CommandManager commandManager) {
        super("set", "set-command-description", "se.set", "<argument-player> <argument-amount>", 2, 2);
        this.commandManager = commandManager;
    }

    @Override
    public void onCommand(CommandSender commandSender, String[] args) {
        IStorage storage = commandManager.getPlugin().getStorageManager().getStorage();
        double amount = CommandUtils.getDouble(args, 1);

        try {
            storage.setPlayerBalance(args[0], amount);
            commandSender.sendMessage(commandManager.getMessagesFile().getMessage("set-command-set")
                    .replace("%amount%", amount + "")
                    .replace("%player%", args[0])
                    .replace("%currency%", commandManager.getPlugin().getStorageManager().getCurrency(amount)));

            DeltaRedisApi deltaRedisApi = DeltaRedisApi.instance();

            deltaRedisApi.sendMessageToPlayer(args[0], commandManager.getMessagesFile().getMessage("set-command-received")
                    .replace("%amount%", amount + "")
                    .replace("%player%", commandSender.getName())
                    .replace("%currency%", commandManager.getPlugin().getStorageManager().getCurrency(amount)));
        } catch (UnknownAccountException e) {
            commandSender.sendMessage(commandManager.getMessagesFile().getMessage("unknown-player"));
        }
    }
}
