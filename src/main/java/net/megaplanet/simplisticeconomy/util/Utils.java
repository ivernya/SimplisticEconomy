package net.megaplanet.simplisticeconomy.util;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

public class Utils {
    private final static int CENTER_PX = 154;

    /**
     * Gets an enum element from its name
     *
     * @param enumClass  the enum class
     * @param enumString the element's name
     * @param <E>        the element type
     *
     * @return the element
     */
    public static <E extends Enum<E>> E getEnumValueFromString(Class<E> enumClass, String enumString) {
        for (E enumElement : EnumSet.allOf(enumClass)) {
            if (enumElement.name().equalsIgnoreCase(enumString)) {
                return enumElement;
            }
        }

        return null;
    }


    /* Originally found on StackOverflow: http://stackoverflow.com/a/2581754/1849152 */
    public static <K, V extends Comparable<? super V>> LinkedHashMap<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list =
                new LinkedList<>(map.entrySet());

        list.sort((o1, o2) -> -((o1.getValue()).compareTo(o2.getValue())));

        LinkedHashMap<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    /**
     * "Skip" the given number of items in a LinkedHashMap and return a new LinkedHashMap with the remaining items.
     *
     * @param map   Map
     * @param nSkip Number of items to skip
     * @return New LinkedHashMap, may be empty.
     */
    @SuppressWarnings("CollectionDeclaredAsConcreteClass")
    public static <K, V> LinkedHashMap<K, V> skip(LinkedHashMap<K, V> map, int nSkip) {
        LinkedHashMap<K, V> newMap = new LinkedHashMap<>();

        if (map.size() <= nSkip) {
            return newMap;
        }

        int i = 0;

        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (i >= nSkip) {
                newMap.put(entry.getKey(), entry.getValue());
            }

            i++;
        }

        return newMap;
    }

    public static <K, V> LinkedHashMap<K, V> take(LinkedHashMap<K, V> map, int nTake) {
        LinkedHashMap<K, V> newMap = new LinkedHashMap<>();

        if (map.size() <= nTake) {
            return map;
        }

        int i = 0;

        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (i >= nTake) {
                break;
            }

            newMap.put(entry.getKey(), entry.getValue());

            i++;
        }

        return newMap;
    }

    public static <K, V> LinkedHashMap<K, V> skipAndTake(LinkedHashMap<K, V> map, int nSkip, int nTake) {
        return take(skip(map, nSkip), nTake);
    }

    public static void sendCenteredMessage(CommandSender sender, String message) {
        if(message == null || message.equals("")) sender.sendMessage("");
        message = ChatColor.translateAlternateColorCodes('&', message);

        int messagePxSize = 0;
        boolean previousCode = false;
        boolean isBold = false;

        for(char c : message.toCharArray()) {
            if(c == '§'){
                previousCode = true;
            }else if(previousCode){
                previousCode = false;
                isBold = c == 'l' || c == 'L';
            }else{
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                messagePxSize++;
            }
        }

        int halvedMessageSize = messagePxSize / 2;
        int toCompensate = CENTER_PX - halvedMessageSize;
        int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
        int compensated = 0;
        StringBuilder sb = new StringBuilder();
        while(compensated < toCompensate) {
            sb.append(" ");
            compensated += spaceLength;
        }
        sender.sendMessage(sb.toString() + message);
    }
}
