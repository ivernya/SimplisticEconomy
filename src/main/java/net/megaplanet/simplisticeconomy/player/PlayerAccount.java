package net.megaplanet.simplisticeconomy.player;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.redisson.api.annotation.REntity;

import java.io.Serializable;

/**
 * The class responsible for storing the player's account information
 */
public class PlayerAccount implements Serializable {
    @JsonProperty("accountHolder")
    private final String accountHolder;

    @JsonProperty("balance")
    private double balance;

    @JsonCreator
    public PlayerAccount(@JsonProperty("accountHolder") String accountHolder, @JsonProperty("balance") double balance) {
        this.accountHolder = accountHolder;
        this.balance = balance;
    }

    /**
     * Gets the holder of this account
     *
     * @return the account's holder
     */

    @JsonGetter("accountHolder")
    public String getAccountHolder() {
        return accountHolder;
    }

    /**
     * Gets the account's balance
     *
     * @return the account's balance
     */

    @JsonGetter("balance")
    public double getBalance() {
        return balance;
    }

    /**
     * Sets the account's balance
     * <p>
     * Must be used exclusively by the storage implementation
     *
     * @param balance the new balance
     */

    @JsonSetter("balance")
    public void setBalance(double balance) {
        this.balance = balance;
    }
}
