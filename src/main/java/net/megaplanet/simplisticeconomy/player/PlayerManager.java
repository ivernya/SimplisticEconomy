package net.megaplanet.simplisticeconomy.player;

import com.gmail.tracebachi.DeltaRedis.Spigot.DeltaRedisApi;
import net.megaplanet.simplisticeconomy.SimplisticEconomy;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerManager implements Listener {

    private final SimplisticEconomy plugin;

    public PlayerManager(SimplisticEconomy plugin) {
        this.plugin = plugin;
    }

    public void load() {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerLogin(AsyncPlayerPreLoginEvent event) {
        plugin.getStorageManager().getStorage().loadAccount(event.getName());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        DeltaRedisApi api = DeltaRedisApi.instance();

        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            String playerName = event.getPlayer().getName();

            api.findPlayer(playerName, (cachedPlayer) -> {
                if(cachedPlayer == null) {
                    plugin.getStorageManager().getStorage().unloadAccount(playerName);
                }
            });

        }, 60L);
    }
}
