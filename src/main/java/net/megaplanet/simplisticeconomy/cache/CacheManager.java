package net.megaplanet.simplisticeconomy.cache;

import com.gmail.tracebachi.DeltaRedis.Spigot.DeltaRedisApi;
import net.megaplanet.simplisticeconomy.SimplisticEconomy;
import net.megaplanet.simplisticeconomy.files.ConfigFile;
import net.megaplanet.simplisticeconomy.player.PlayerAccount;
import org.bukkit.Bukkit;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.Config;

import java.util.Map;

public class CacheManager {
    private final SimplisticEconomy plugin;
    private RedissonClient client;

    public CacheManager(SimplisticEconomy plugin) {
        this.plugin = plugin;
    }

    public void load() {
        ConfigFile config = plugin.getFileManager().getConfigFile();

        String host = config.getString("storage.redis.host");
        int port = config.getInt("storage.redis.port");
        String password = config.getString("storage.redis.password");

        Config redisConfig = new Config();
        if (password == null) {
            redisConfig.useSingleServer().setAddress("redis://" + host + ":" + port);
        } else {
            redisConfig.useSingleServer().setAddress("redis://" + host + ":" + port).setPassword(password);
        }

        redisConfig.setCodec(new JsonJacksonCodec());

        client = Redisson.create(redisConfig);

        RMap<String, PlayerAccount> players = plugin.getCacheManager().getClient().getMap("loadedPlayers");

        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
            @Override
            public void run(){
                DeltaRedisApi api = DeltaRedisApi.instance();

                for (Map.Entry<String, PlayerAccount> entry : players.entrySet()) {
                    String player = entry.getKey();

                    api.findPlayer(player, (cachedPlayer) -> {
                        if (cachedPlayer == null) {
                            plugin.getStorageManager().getStorage().unloadAccount(player);
                        }
                    });
                }
            }
        });
    }

    public RedissonClient getClient() {
        return client;
    }
}
