package net.megaplanet.simplisticeconomy.vault;

import net.megaplanet.simplisticeconomy.SimplisticEconomy;
import net.megaplanet.simplisticeconomy.storage.TransactionResponse;
import net.megaplanet.simplisticeconomy.storage.TransactionResponseType;
import net.megaplanet.simplisticeconomy.storage.UnknownAccountException;
import net.milkbowl.vault.economy.AbstractEconomy;
import net.milkbowl.vault.economy.EconomyResponse;

import java.text.DecimalFormat;

public class VaultIntegration extends AbstractEconomy implements UnsupportedAccountNameEconomy, UnsupportedBankEconomy {

    private final SimplisticEconomy plugin;
    private static DecimalFormat df2 = new DecimalFormat("###,###.00");

    public VaultIntegration(SimplisticEconomy plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean isEnabled() {
        return plugin.isEnabled();
    }

    @Override
    public String getName() {
        return plugin.getName();
    }

    @Override
    public int fractionalDigits() {
        return 2;
    }

    @Override
    public String format(double amount) {
        return df2.format(amount).replaceAll("\u00A0"," ");
    }

    @Override
    public String currencyNamePlural() {
        return plugin.getStorageManager().getCurrencyPlural();
    }

    @Override
    public String currencyNameSingular() {
        return plugin.getStorageManager().getCurrencySingular();
    }

    @Override
    public boolean hasAccount(String playerName) {
        return plugin.getStorageManager().getStorage().hasAccount(playerName);
    }

    @Override
    public double getBalance(String playerName) {
        try {
            return plugin.getStorageManager().getStorage().getBalance(playerName);
        } catch (UnknownAccountException e) {
            return 0;
        }
    }

    @Override
    public boolean has(String playerName, double amount) {
        try {
            return plugin.getStorageManager().getStorage().hasEnough(playerName, amount);
        } catch (UnknownAccountException e) {
            return false;
        }
    }

    @Override
    public EconomyResponse withdrawPlayer(String playerName, double amount) {
        try {
            return transform(plugin.getStorageManager().getStorage().withdrawPlayer(playerName, amount));
        } catch (UnknownAccountException e) {
            return new EconomyResponse(amount, 0, EconomyResponse.ResponseType.FAILURE, "Account not found");
        }
    }

    @Override
    public EconomyResponse depositPlayer(String playerName, double amount) {
        try {
            TransactionResponse transactionResponse = plugin.getStorageManager().getStorage().depositPlayer(playerName, amount);

            return transform(transactionResponse);
        } catch (UnknownAccountException e) {
            return new EconomyResponse(amount, 0, EconomyResponse.ResponseType.FAILURE, "Account not found");
        }
    }

    @Override
    public boolean createPlayerAccount(String playerName) {
        return plugin.getStorageManager().getStorage().createAccount(playerName);
    }

    private EconomyResponse transform(TransactionResponse transactionResponse) {
        EconomyResponse.ResponseType responseType = transactionResponse.getTransactionResponseType() == TransactionResponseType.SUCCESS ? EconomyResponse.ResponseType.SUCCESS : EconomyResponse.ResponseType.FAILURE;
        return new EconomyResponse(transactionResponse.getAmount(), transactionResponse.getBalance(), responseType, transactionResponse.getFailureReason());
    }
}
