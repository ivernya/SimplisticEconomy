package net.megaplanet.simplisticeconomy.storage;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Map;

public interface IStorage {

    void loadAccount(String player);

    void unloadAccount(String player);

    TransactionResponse depositPlayer(String player, double amount) throws UnknownAccountException;

    TransactionResponse setPlayerBalance(String player, double amount) throws UnknownAccountException;

    TransactionResponse withdrawPlayer(String player, double amount) throws UnknownAccountException;

    default boolean hasEnough(String player, double amount) throws UnknownAccountException {
        return getBalance(player) >= amount;
    }

    double getBalance(String player) throws UnknownAccountException;

    /*
       For vault
     */
    default boolean createAccount(String player) {
        loadAccount(player);
        return true;
    }

    boolean hasAccount(String player);

    Map<String, Double> getTopBalance(int amount, int offset);

    void enableStorage();

    void disableStorage();

}
