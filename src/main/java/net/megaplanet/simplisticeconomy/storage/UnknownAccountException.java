package net.megaplanet.simplisticeconomy.storage;

public class UnknownAccountException extends Throwable {
    public UnknownAccountException() {
        super("Unable to find this account");
    }
}
