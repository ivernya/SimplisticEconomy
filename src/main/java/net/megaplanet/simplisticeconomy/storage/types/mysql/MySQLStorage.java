package net.megaplanet.simplisticeconomy.storage.types.mysql;

import net.megaplanet.simplisticeconomy.cache.CacheManager;
import net.megaplanet.simplisticeconomy.player.PlayerAccount;
import net.megaplanet.simplisticeconomy.storage.IStorage;
import net.megaplanet.simplisticeconomy.storage.StorageManager;
import net.megaplanet.simplisticeconomy.storage.TransactionResponse;
import net.megaplanet.simplisticeconomy.storage.UnknownAccountException;
import net.megaplanet.simplisticeconomy.util.Utils;
import org.bukkit.entity.Player;
import org.redisson.api.RMap;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class MySQLStorage implements IStorage {
    private final StorageManager storageManager;
    private final CacheManager cacheManager;
    private final MySQLConnectionHandler connectionHandler;
    private final RMap<String, PlayerAccount> loadedPlayers;
    private String tableName;

    public MySQLStorage(StorageManager storageManager, CacheManager cacheManager) {
        this.storageManager = storageManager;
        this.cacheManager = cacheManager;

        loadedPlayers = cacheManager.getClient().getMap("loadedPlayers");

        this.connectionHandler = new MySQLConnectionHandler(storageManager.getPlugin());
        this.tableName = storageManager.getPlugin().getFileManager().getConfigFile().getString("storage.table");
    }

    @Override
    public void loadAccount(String player) {
        String finalPlayer = player.toLowerCase();

        connectionHandler.executeSQLQuery(connection -> {
            if (loadedPlayers.containsKey(finalPlayer)) {
                return;
            }

            PlayerAccount playerAccount = loadAccount(player, connection);
            loadedPlayers.put(finalPlayer, playerAccount);
        });
    }

    private PlayerAccount loadAccount(String player, Connection connection) throws SQLException {
        PreparedStatement getAccountStatement = connection.prepareStatement(replaceTable(Queries.SELECT));
        getAccountStatement.setString(1, player);
        ResultSet resultSet = getAccountStatement.executeQuery();

        double balance;

        if(resultSet.next()) {
            balance = resultSet.getDouble("balance");
        } else {
            balance = storageManager.getStartingBalance();
            PreparedStatement insertRecord = connection.prepareStatement(replaceTable(Queries.INSERT));
            insertRecord.setString(1, player.toString());
            insertRecord.setDouble(2, balance);
            insertRecord.execute();
            insertRecord.close();
        }

        resultSet.close();
        getAccountStatement.close();
        return new PlayerAccount(player, balance);
    }

    @Override
    public void unloadAccount(String player) {
        player = player.toLowerCase();

        loadedPlayers.remove(player);
    }

    @Override
    public TransactionResponse depositPlayer(String player, double amount) throws UnknownAccountException {
        String finalPlayer = player.toLowerCase();

        double initialBalance = getBalance(finalPlayer);

        if(loadedPlayers.containsKey(finalPlayer)) {
            PlayerAccount account = loadedPlayers.get(finalPlayer);
            account.setBalance(initialBalance + amount);

            loadedPlayers.put(finalPlayer, account);
        }

        connectionHandler.executeSQLQuery(connection -> {
            PreparedStatement updateBalance = connection.prepareStatement(replaceTable(Queries.UPDATE_ADD));
            updateBalance.setDouble(1, amount);
            updateBalance.setString(2, finalPlayer);
            updateBalance.execute();
            updateBalance.close();
        }, loadedPlayers.containsKey(finalPlayer));

        return TransactionResponse.createSuccessResponse(amount, initialBalance + amount);
    }

    @Override
    public TransactionResponse setPlayerBalance(String player, double amount) throws UnknownAccountException {
        String finalPlayer = player.toLowerCase();

        if(!hasAccount(finalPlayer)) {
            throw new UnknownAccountException();
        }

        if(loadedPlayers.containsKey(finalPlayer)) {
            PlayerAccount account = loadedPlayers.get(finalPlayer);
            account.setBalance(amount);

            loadedPlayers.put(finalPlayer, account);
        }

        connectionHandler.executeSQLQuery(connection -> {
            PreparedStatement updateBalance = connection.prepareStatement(replaceTable(Queries.UPDATE_SET));
            updateBalance.setDouble(1, amount);
            updateBalance.setString(2, finalPlayer);
            updateBalance.execute();
            updateBalance.close();
        }, loadedPlayers.containsKey(finalPlayer));

        return TransactionResponse.createSuccessResponse(amount, amount);
    }

    @Override
    public TransactionResponse withdrawPlayer(String player, double amount) throws UnknownAccountException {
        String finalPlayer = player.toLowerCase();

        TransactionResponse transactionResponse;
        double initialBalance = getBalance(finalPlayer);

        if(initialBalance < amount) {
            transactionResponse = TransactionResponse.createFailureResponse("Insufficient funds", amount, initialBalance);
        } else {
            double newBalance = initialBalance - amount;
            if(loadedPlayers.containsKey(finalPlayer)) {
                PlayerAccount account = loadedPlayers.get(finalPlayer);
                account.setBalance(newBalance);

                loadedPlayers.put(finalPlayer, account);
            }

            connectionHandler.executeSQLQuery(connection -> {
                PreparedStatement updateBalance = connection.prepareStatement(replaceTable(Queries.UPDATE_REMOVE));
                updateBalance.setDouble(1, amount);
                updateBalance.setString(2, finalPlayer);
                updateBalance.execute();
                updateBalance.close();
            }, loadedPlayers.containsKey(finalPlayer));
            transactionResponse = TransactionResponse.createSuccessResponse(amount, newBalance);
        }

        return transactionResponse;
    }

    @Override
    public double getBalance(String player) throws UnknownAccountException {
        String finalPlayer = player.toLowerCase();

        if(!hasAccount(finalPlayer)) {
            throw new UnknownAccountException();
        }

        if (loadedPlayers.containsKey(finalPlayer)) {
            AtomicReference<PlayerAccount> playerAccount = new AtomicReference<>(loadedPlayers.get(finalPlayer));

            return playerAccount.get().getBalance();
        } else {
            AtomicReference<Double> balance = new AtomicReference<>((double) 0);

            connectionHandler.executeSQLQuery(connection -> {
                PreparedStatement getAccountStatement = connection.prepareStatement(replaceTable(Queries.SELECT));
                getAccountStatement.setString(1, finalPlayer);
                ResultSet resultSet = getAccountStatement.executeQuery();

                if(resultSet.next()) {
                    balance.set(resultSet.getDouble("balance"));
                } else {
                    balance.set((double) 0);
                }
            });

            return balance.get();
        }
    }

    @Override
    public boolean hasAccount(String player) {
        String finalPlayer = player.toLowerCase();

        AtomicBoolean atomicBoolean = new AtomicBoolean(false);

        if(loadedPlayers.containsKey(finalPlayer)) {
            return true;
        }

        connectionHandler.executeSQLQuery(connection -> {
            PreparedStatement getAccountStatement = connection.prepareStatement(replaceTable(Queries.SELECT));
            getAccountStatement.setString(1, finalPlayer);
            ResultSet resultSet = getAccountStatement.executeQuery();

            if(resultSet.next()) {
                atomicBoolean.set(true);
            }

            resultSet.close();
            getAccountStatement.close();
        });

        return atomicBoolean.get();
    }

    @Override
    public Map<String, Double> getTopBalance(int amount, int offset) {
        LinkedHashMap<String, Double> top = new LinkedHashMap<>();

        connectionHandler.executeSQLQuery(connection -> {
            PreparedStatement getAccountStatement = connection.prepareStatement(replaceTable(Queries.SELECT_TOP));
            ResultSet resultSet = getAccountStatement.executeQuery();

            while (resultSet.next()) {
                top.put(resultSet.getString("player_name"), resultSet.getDouble("balance"));
            }

            resultSet.close();
            getAccountStatement.close();
        });

        return Utils.skipAndTake(top, offset, amount);
    }

    private String replaceTable(String statement) {
        return statement.replace("%TABLE%", tableName);
    }

    @Override
    public void enableStorage() {
        connectionHandler.setupHikari();
    }

    @Override
    public void disableStorage() {
        connectionHandler.closeHikari();
    }

    public String getTableName() {
        return tableName;
    }
}
